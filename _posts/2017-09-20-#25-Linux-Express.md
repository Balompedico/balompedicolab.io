---
title: "#25 Linux Express"
date: 2017-10-04
author: juan
category: [linuxexpress]
featimg: 2017/25LinuxExpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2325%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/25LinuxExpress.png)
Semanalmente alternamos un episodio formal con estos Linux Express, para que conozcas qué se cuece en Podcast Linux.  

<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2325%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #35 Formatos Libres](http://avpodcast.net/podcastlinux/formatoslibres).
+ Próximo Linux Connexion con Atareao.
+ Octubre, mes del podcasting:
   + [International Podcast Day](https://internationalpodcastday.com/)
   + [Killall Radio Podcast Day](https://neositelinux.com/podcast-celebrando-el-dia-internacional-del-podcast/)
   + [Podcast Action Day](https://www.oxfamintermon.org/minisites/podcast_action_day/)
   + [Día del Podcast](http://diadelpodcast.com/)
   + [Jpod Alicante 2017](https://jpod.es/)
+ Curso: crea tu propio podcast: [Archive.org](https://archive.org/details/CursoPodcasting) y [Youtube](https://youtu.be/a6dCJpjLH3Q)

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
