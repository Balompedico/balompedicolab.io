---
title: "#13 Linux Express"
author: juan
date: 2017-05-17
category: linuxexpress
featimg: 2017/13LinuxExpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2313%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/13LinuxExpress.png)  
Aquí tienes otra entrega de Linux Express, el podcast creado de los audios de Telegram, para que estés informado de lo que se cuece en Podcast Linux.

<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2313%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>

Hoy quiero compartir contigo estos temas:

+ [Episodio #24 Linux Connexion con David OchoBits](http://avpodcast.net/podcastlinux/davidochobits)
+ Próximo episodio GNU/Linux y NAS
+ [Slimbook One](https://slimbook.es/one-minipc-potente)
+ [Post KDE Blog](https://kdeblog.com/podcast-linux-y-la-educacion.html)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.github.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://feedpress.me/podcastlinux>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.gitlab.io/Linux-Express/feed>
